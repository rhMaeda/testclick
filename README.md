# Teste
### Passo a passo
Copiar arquivo .env.example para .env
```sh
$ cd testeclick
$ php artisan key:generate
```
Pegar a chave gerada e substituir na APP_KEY em .env
Configurar acesso do DB no .env

Criar banco com nome "testeclick"
```sh
$ php artisan migrate
```
Para alimentar o banco com frutas:
```sh
$ php artisan db:seed
```
Para iniciar o servidor
```sh
$ php artisan serve
```
* PHP
* CSS
* Bootstrap
* HTML
* Javascript
* VueJS
* Axios
* MySQL
* Blade
* Carbon
* Eloquent
