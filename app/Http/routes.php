<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
	Route::get('/', function () {
		return view('welcome');
	});
	//Produtos inicio
	Route::get('/produtos','ProdutoController@lista');
	
	Route::get(
		'/produtos/mostra/{id}',
		'ProdutoController@mostra'
		)
	->where('id', '[0-9]+');
	
	Route::get('/produtos/novo', 'ProdutoController@novo');
	
	Route::post('/produtos/adiciona', 'ProdutoController@adiciona');
	
	Route::get('/produtos/remove/{id}', 'ProdutoController@remove');

	Route::get('/produtos/edita/{id}', 'ProdutoController@edita');
	
	Route::post('/produtos/altera', 'ProdutoController@altera');

	//produtos fim
	//vendas inicio
	Route::get('/vendas','VendaController@lista');
	Route::get('/vendas/novo', 'VendaController@novo');
	Route::post('/vendas/adiciona', 'VendaController@adiciona');
	Route::get('/vendas/test', 'VendaController@getProdutos');
	
	//vendas fim

	Route::get('/usuario', 'UserController@index');
	Route::get('/usuario/novo', 'UserController@novo');
	Route::get('/listausuario', 'UserController@getCustomer');
	Route::post('/novousuario', 'UserController@adduser');
	Route::post('/usuario/adiciona', 'UserController@adiciona');

	Route::get('home', 'HomeController@index');
	
	Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
	]);
	
	Route::get('/login', 'LoginController@login');
	