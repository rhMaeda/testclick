<?php

namespace estoque\Http\Controllers;

use Illuminate\Http\Request;

use estoque\Http\Requests;
use estoque\Http\Controllers\Controller;
use estoque\Venda;
use estoque\Produto;
use estoque\User;
use DB;
use Carbon\Carbon;
use estoque\Http\Requests\ProdutosRequest;

class VendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function lista(){
        $vendas = Venda::all();//Eloquent - usando modelo produto
		$err;
		if (view()->exists('venda.listagem')){
		
			return view('venda.listagem')->with('vendas', $vendas);
		}
    }
    public function getProdutos(){
        $listProduto = Produto::all();
        return $listProduto->toJson(JSON_PRETTY_PRINT);
    }
    public function getCustomer(){
        $listUser = User::all();
        return $listUser->toJson(JSON_PRETTY_PRINT);
    }
    public function novo(){
        $lista = $this->getProdutos();
        $usuarios = $this->getCustomer();
        return view('venda.formulario')->with('produtos', $lista)->with('usuarios', $usuarios);
        //
    }
    public function adiciona(Request $request){
        $quantidade = $request->quantidade;
        $cliente = $request->cliente;
        $fruta = $request->nome; //nome mas é o id da fruta
        if($cliente == "Selecione" || $fruta == "Selecione"){
            $lista = $this->getProdutos();
            $usuarios = $this->getCustomer();
            $errinho = "Preencher corretamente";
            return view('venda.formulario')->with('produtos', $lista)->with('usuarios', $usuarios)->with('err', $errinho);
        }
        //select quantidade de frutas
        //$selectqtd = DB::table('produtos')->select('quantidade')->where('id', $fruta)->get();
        $selectqtd = Produto::select('quantidade')->where('id', $fruta)->first();
        
        $selectqtd = $selectqtd->quantidade;
        //$return = implode(",", array_column($selectqtd, "quantidade"));
 
        //select valor da fruta
        //$selectvalor = DB::table('produtos')->select('valor')->where('id', $fruta)->get();
        $selectvalor = Produto::select('valor')->where('id', $fruta)->first();
        $selectvalor = $selectvalor->valor;
        //$val = implode(",", array_column($selectvalor, "valor"));

        //calculo valor venda
        $valorVenda = $quantidade*$selectvalor;
        //calculo quantidade restante
        $qtd = $selectqtd - $quantidade;
   
        if($qtd < 0){
            $vendas = Venda::all();//Eloquent - usando modelo produto
            $errinho = "Estoque abaixo da qtd solicitada";
		    if (view()->exists('venda.listagem')){
		
			return view('venda.listagem')->with('vendas', $vendas)->with('err', $errinho);
		}
        } else{
        //Venda::select('produtos')->where('id', $fruta)->get();
        Produto::where('id', $fruta)->update(['quantidade' => $qtd]);
        //DB::table('produtos')->where('id', $fruta)->update(['quantidade' => $qtd]);
        $inp = ['cliente' =>  $cliente,'valor' => $valorVenda, 'data_venda'=> Carbon::now()->format('d-m-Y'), 'quantidade' => $quantidade];
        Venda::create($inp);
        // DB::table('vendas')->insert(
		// 	['cliente' => $cliente,
		// 	'valor' => $valorVenda,
		// 	'data_venda' => Carbon::now()->format('d-m-Y'),
		// 	'quantidade' => $quantidade
		// 	]
        // );
        return $this->lista();
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
