<?php

namespace estoque;

use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
    protected $table = 'vendas';
    public $timestamps = false;
    protected $fillable = ['cliente', 'quantidade', 'valor', 'data_venda', 'updated_at'];
}
