@extends('layout.usersmenu')

		@section('conteudo')

				<h1>Usuario</h1>
                <div id="appl">
                <form>
                <label>Nome</label>
                <input type="text" class="form-control"  name="nomenovo" id="n" required placeholder="Nome Completo" v-model="novousuario.nome">
                <label>cpf</label>
                <input type="text" class="form-control"  name="cpfnovo" id="c" required placeholder="cpf" v-model="novousuario.cpf">
                <label>Email</label>
                <input type="text" class="form-control"  name="emailnovo" id="e" required placeholder="email" v-model="novousuario.email">
                </br>
                <button type="button" @click="addnovousuario" class="btn btn-primary">Add</button>
                </form>
                <table class="table table-striped table-bordered table-hover">
                    <tr>
                        <th>Cliente</th>
                        <th>CPF</th> 
                        <th>Email</th>
       
                    </tr> 
					
                    
					<tr v-for="usuario in listUser">
						<td>@{{ usuario.name }} </td>
                        <td>@{{ usuario.cpf }} </td>
                        <td>@{{ usuario.email }} </td>

					</tr>

				</table>


                </div>

			<script src="/js/vue.js"></script>
            <script src="/js/axios.min.js"></script>
            <script>
            function transforReq(){
                this.transformRequest = (data, headers) => {
                if (data && typeof data === 'object'){
                    let formData = new FormData()
                    Object.keys(data).forEach(key => {
                        formData.append(key, data[key])
                    })
                    return formData
                }else{
                    return data
                }
            }
            }
            window.onload = function () {
                Vue.prototype.$http = axios.create({
                    baseURL: '<?php URL::to('/'); ?>',
                    transformRequest: transforReq()
	            })
                const app = new Vue({
                    el: '#appl',
                    data: {
                        teste: "oi",
                        listUser: [],
                        nome: '',
                        cpf: '',
                        novousuario:{
                            nome:"",
                            cpf: "",
                            email: "",
                        },

                    },
                    filters: {
                    },
                    computed: {
                    },
                    mounted() {  
                        const vm = this  
                        vm.listaUsuarios()
                        
                    },
                    methods:{
                        listaUsuarios(){
                            const vm = this
                            axios.get('/listausuario').then((response)=>
                            vm.listUser = response.data

                            )
                        },
                        addnovousuario(){
                            const vm = this
                            let data = {
                                nomenovo: vm.novousuario.nome,
                                cpfnovo: vm.novousuario.cpf,
                                emailnovo: vm.novousuario.email
                            }
                            axios.post('/novousuario', data).then(()=>{
                                vm.listaUsuarios();
                                vm.novousuario.nome = ""
                                vm.novousuario.cpf = ""
                                vm.novousuario.email = ""
                            })

                        }

                    }
                })
            }
             
            </script>


			@stop
            