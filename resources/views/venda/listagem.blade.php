@extends('layout.menuVendas')

		@section('conteudo')
		    @if(isset($err))
				<div class="alert-danger">
					<?php echo $err;?>
				</div>
            
			@endif
			@if(empty($vendas))
				<div class="alert-danger">
					Você não tem nenhum produto cadastrado.
				</div>
			
			@else
				<h1>Listagem de vendas</h1>
                <div id="app">
				<table class="table table-striped table-bordered table-hover">
                <tr>
                        <th>Cliente</th>
                        <th>Valor da venda</th> 
                        <th>Data da venda</th>
                        <th>Quantidade frutas</th>
                    </tr>
					@foreach ($vendas as $p)
                    
					<tr class="{{$p->quantidade<=1 ? 'danger' : '' }}">

						<td>{{ $p->cliente }} </td>
						<td>R${{ $p->valor }} </td>
						<td>{{ $p->data_venda }} </td>
						<td>{{ $p->quantidade }} </td>
						
						
						
					</tr>
					@endforeach
				</table>
                </div>
			@endif
			
			@if(old('nome'))
				<div class="alert alert-success">
					<strong> Sucesso!</strong> O produto {{old('nome')}} foi adicionado com sucesso!
				</div>
				
			@endif

			@stop
            