@extends('layout.menuVendas')
	
	@section('conteudo')
	
		<h1>Venda</h1>
            @if(isset($err))
				<div class="alert-danger">
					<?php echo $err;?>
				</div>
            
			@endif
			@if (count($errors) > 0)	
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
	
			<form action="/vendas/adiciona" method="post" id="app">
			
				<input type="hidden"
					name="_token" value="{{{ csrf_token() }}}" />
			
				<div class="form-group">
					<label>Cleinte</label>
					<select class="form-control" name="cliente">
                    <option>Selecione</option>
                    <option v-for="cliente in listusuarios" :value="cliente.name">
                       @{{ cliente.name }}
                    </option>
                    </select>
				</div>
                <label>Nome</label>
                <div class="form-group">
					
                    <select class="form-control" name="nome">
                    <option>Selecione</option>
                    <option v-for="prod in listProdutos" :value="prod.id" v-if="prod.quantidade > 0">
            
                    @{{ prod.nome }} (@{{prod.quantidade}} no estoque)
                  
                       
                    </option>
                    </select>
				</div>
                <div class="form-group">
					<label>Quantidade</label>
					<input type="number" class="form-control" name="quantidade" value="{{ old('quantidade') }}">
				</div>
				<button type="submit" class="btn
				btn-primary btn-block">Submit</button>
				
			</form>
            <script src="/js/vue.js"></script>
            <script src="/js/axios.min.js"></script>
            <script>
            function transforReq(){
                this.transformRequest = (data, headers) => {
                if (data && typeof data === 'object'){
                    let formData = new FormData()
                    Object.keys(data).forEach(key => {
                        formData.append(key, data[key])
                    })
                    return formData
                }else{
                    return data
                }
            }
            }
            window.onload = function () {
                Vue.prototype.$http = axios.create({
                    baseURL: '<?php URL::to('/'); ?>',
                    transformRequest: transforReq()
	            })
                const app = new Vue({
                    el: '#app',
                    data: {
                        teste: "oi",
                        listProdutos: <?= $produtos ?>,
                        listusuarios: <?= $usuarios ?>,
                    },
                    filters: {
                    },
                    computed: {
                    },
                    mounted() {  
                        const vm = this  
                        console.log(vm.listProdutos)
                        console.log(vm.listusuarios)
                        
                    },
                    methods:{
                        
                    }
                })
            }
             
            </script>
		
	@stop
	