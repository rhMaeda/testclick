	@extends('layout.principal')
	
	@section('conteudo')
		@if(isset($p))
		<h1>Edita Produto {{$p->nome}}</h1>
		@else
		<h1>Novo produto</h1>
		@endif		
			@if (count($errors) > 0)	
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
	
			@if(!isset($p->id))
            <form method = "post" action="/produtos/adiciona">
            @else
             <form method = "post" action="/produtos/altera">
            @endif

				<input type="hidden"
					name="_token" value="{{{ csrf_token() }}}" />

				@if(isset($p->id))
				<input type="hidden" name="id" value="{{$p->id}}" />	
				@endif


				<div class="form-group">
					<label>Nome</label>
					<input class="form-control" name="nome" value="{{ old('nome', $p->nome ?? null) }}">
				</div>
				<div class="form-group">
					<label>Validade</label>
					<input class="form-control" type="date" name="valid" value="{{ old('valid', $p->valid ?? null) }}">
				</div>
				<div class="form-group">
					<label>Valor</label>
					<input class="form-control" name=valor value="{{ old('valor', $p->valor ?? null) }}">
				</div>
				<div class="form-group">
					<label>Quantidade</label>
					<input type="number" class="form-control" name="quantidade" value="{{ old('quantidade', $p->quantidade ?? null) }}">
				</div>
				
				<button type="submit" class="btn
				btn-primary btn-block">Submit</button>
				
			</form>
		
	@stop
	