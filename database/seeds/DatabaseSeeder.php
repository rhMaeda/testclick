<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()	
    {
        Model::unguard();

        $this->call('ProdutoTableSeeder');

        Model::reguard();
    }
}

class ProdutoTableSeeder extends Seeder {
	public function run()
	{
		DB::insert('insert into produtos
		(nome, quantidade, valor, valid)
		values (?,?,?,?)',
		array('Maça', 10, 12.00,
        '2020-10-10'));
        DB::insert('insert into produtos
		(nome, quantidade, valor, valid)
		values (?,?,?,?)',
		array('Pera', 15, 22.00,
        '2020-10-12'));
        DB::insert('insert into produtos
		(nome, quantidade, valor, valid)
		values (?,?,?,?)',
		array('Banana', 5, 10.00,
		'2020-09-01'));
	}
}
